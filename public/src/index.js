var $ = require('jquery')
// Bootstrap wants jQuery global =(
window.jQuery = $

require('@popperjs/core/dist/umd/popper')
require('bootstrap/dist/js/bootstrap')

var cssify = require('cssify')
cssify.byUrl('./dist/css/bootstrap.min.css')
cssify.byUrl('./dist/css/style.css')

